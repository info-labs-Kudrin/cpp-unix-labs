// Библиотека для вывода на экран
#include <iostream>
using namespace std;

// Функция для сложения
int add(int a, int b) {
    return a + b;
}

// Функция для умножения
int subtract(int a, int b) {
    return a - b;
}

// Функция для возведения в степень
int power(int a, int b) {
    int result = 1;
    for (int i = 0; i < b; i++) {
        result *= a;
    }
    return result;
}

int main(int argc, char* argv[]) {
    // Числа
    int a, b;
    // Оператор
    char op;

    // Проверяем, что пользователь подал 3 аргумента в функцию
    if (argc != 4) {
        cout << "Wrong number of arguements!" << endl;
        return 1;
    }
    // Если пользователь подал не числа, stoi выдаст ошибку, которую мы и ловим
    try {
        a = stoi(argv[1]);
        b = stoi(argv[2]);

        op = argv[3][0];

        switch (op) {
        case '+': {
            cout << "Answer: " << add(a, b) << endl;
            break;
        }
        case '-': {
            cout << "Answer: " << subtract(a, b) << endl;
            break;
        }
        case '^': {
            cout << "Answer: " << power(a, b) << endl;
            break;
        }
        default:
            cout << "Incorrect operator!" << endl;
        }
    }
    catch (...) {
        cout << "Something went wrong (most likely, you wrote not a number or the number is too big)";
    }
    return 0;
}
