#include <iostream>
#include <string>
using namespace std;

string getString(int from, int to) {
    string res = "";
    res.push_back(from + '0');
    res.push_back('-');
    res.push_back('>');
    res.push_back(to + '0');
    return res;
}

class Node {
public:
    string data;
    Node* next;
    Node* prev;

    Node(string value) {
        data = value;
        next = nullptr;
        prev = nullptr;
    }

    ~Node() {
        if (next != nullptr)
            delete next;
    }

};

class LinkedList {
private:
    Node* head;
    Node* tail;

public:
    LinkedList() {
        head = nullptr;
        tail = nullptr;
    }

    // Добавить элемент в начало списка
    void push_front(string value) {
        Node* node = new Node(value);

        if (head == nullptr) {
            head = node;
            tail = node;
        }
        else {
            node->next = head;
            head->prev = node;
            head = node;
        }
    }

    // Добавить элемент в конец списка
    void push_back(string value) {
        Node* node = new Node(value);

        if (tail == nullptr) {
            head = node;
            tail = node;
        }
        else {
            node->prev = tail;
            tail->next = node;
            tail = node;
        }
    }

    // Вывести список
    void print() {
        Node* cur = head;

        while (cur != nullptr) {
            cout << cur->data << endl;
            cur = cur->next;
        }
    }

    Node* getHead() {
        return head;
    }

    ~LinkedList() {
        delete head;
    }

};


class HannoiTower {
private:
    int n; // количество дисков
    int* tower1; // первая башня
    int* tower2; // вторая башня
    int* tower3; // третья башня
public:
    HannoiTower(int num) {
        n = num;
        tower1 = new int[n];
        tower2 = new int[n];
        tower3 = new int[n];
        for (int i = 0; i < n; i++) {
            tower1[i] = n - i;
            tower2[i] = 0;
            tower3[i] = 0;
        }
    }
    void printTowers() {
        for (int i = n - 1; i >= 0; i--) {
            // выводим содержимое башен в консоль
            cout << "\t";
            if (tower1[i] != 0) cout << tower1[i]; else cout << "|";
            cout << "\t";
            if (tower2[i] != 0) cout << tower2[i]; else cout << "|";
            cout << "\t";
            if (tower3[i] != 0) cout << tower3[i]; else cout << "|";
            cout << endl;
        }
        cout << "\tA\tB\tC" << endl;
    }
    bool move(int from, int to) {
        // проверка на дурака
        if (from < 1 || from > 3 || to < 1 || to > 3 || from == to) return false;
        // находим верхний диск на башне from
        int topDisk = 0;
        int* fromTower = NULL;
        int* toTower = NULL;
        switch (from) {
        case 1: fromTower = tower1; break;
        case 2: fromTower = tower2; break;
        case 3: fromTower = tower3; break;
        }
        switch (to) {
        case 1: toTower = tower1; break;
        case 2: toTower = tower2; break;
        case 3: toTower = tower3; break;
        }
        for (int i = 0; i < n; i++) {
            if (fromTower[i] != 0) {
                topDisk = fromTower[i];
                fromTower[i] = 0;
                break;
            }
        }
        // находим верхний диск на башне "to"
        for (int i = n - 1; i >= 0; i--) {
            if (toTower[i] == 0) {
                toTower[i] = topDisk;
                return true;
            }
        }
        // если не удалось переместить диск, возвращаем false
        return false;
    }
    ~HannoiTower() {
        delete[] tower1;
        delete[] tower2;
        delete[] tower3;
    }
};

void hannoiTower(int *towers, int from, int to, LinkedList& steps)
{
    if (towers[from] <= 0)
        throw 1;
    if (towers[from] == 1)
    {
        steps.push_back(getString(from, to));
        towers[from]--;
        towers[to]++;
        return;
    }
    int* tow1 = new int[3];
    tow1[0] = towers[0];
    tow1[1] = towers[1];
    tow1[2] = towers[2];
    tow1[from]--;
    hannoiTower(tow1, from, 3 - to - from, steps);
    steps.push_back(getString(from, to));
    hannoiTower(tow1, 3 - to - from, to, steps);
    towers[to] += towers[from];
    towers[from] = 0;
    delete[] tow1;
}

int main() {
    LinkedList list;
    int n = 4;
    int* t = new int[n];
    t[0] = n; t[1] = 0; t[2] = 0;
    HannoiTower tower(n);
    tower.printTowers();
    cout << endl << endl;

    hannoiTower(t, 0, 1, list);

    Node* cur = list.getHead();
    while (cur != nullptr)
    {
        int from = cur->data[0] - '0' + 1, to = cur->data[3] - '0' + 1;
        tower.move(from, to);
        tower.printTowers();
        cout << endl << endl;
        cur = cur->next;
    }
    return 0;
}
