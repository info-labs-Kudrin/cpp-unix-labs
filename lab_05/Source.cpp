#include <iostream>
#include <fstream>
#include <string>
#include <exception>
#include <thread>
#include <vector>
#include <stack>
#include <chrono>

using namespace std;

ifstream f;
vector<thread> threads;

namespace myLang {

	float toFloat(string s) {
		float res = 0, floating = 0;
		bool toFloating = 0;
		size_t sz = s.size();
		for (int i = 0; i < sz; i++) {
			if (s[i] == '.')
				toFloating = 1;
			else {
				if (s[i] > '9' || s[i] < '0')
					throw exception("incorrect expression");
				if (toFloating)
					floating = floating * 10 + (s[i] - '0');
				else
					res = res * 10 + (s[i] - '0');
			}
		}
		while (abs(floating) >= 1)
			floating /= 10;
		return res + floating;
	}

	string toStr(float f) {
		bool neg = (f < 0);
		f = abs(f);
		int r = trunc(f);
		int i = round(100000*(f - trunc(f)));
		string res = "", fl = "";
		if (r == 0)
			res = "0";
		while (r != 0)
		{
			res = res + char(r % 10 + '0');
			r /= 10;
		}
		reverse(res.begin(), res.end());
		if (i != 0) {
			res += '.';
			while (i != 0) {
				fl = char(i % 10 + '0') + fl;
				i /= 10;
			}
			i = round(100000 * (f - trunc(f)));
			if (i < 10000)
				fl = "0" + fl;
			if (i < 1000)
				fl = "0" + fl;
			if (i < 100)
				fl = "0" + fl;
			if (i < 10)
				fl = "0" + fl;
			res += fl;
		}
		if (neg)
			res = '-' + res;
		return res;
	}

	float getRight(string s, int ind, int& right, int iter = 0) {
		int r = ind + 1;
		if (s[r] == 'i')
			return iter;
		if (s[r] > '9' || s[r] < '0')
			throw exception("Incorrect expression");
		bool dot = 0;
		size_t sz = s.size();
		while (r < sz && (s[r] >= '0' && s[r] <= '9' || s[r] == '.')) {
			if (s[r] == '.')
			{
				if (dot)
					throw exception("Incorrect expression");
				dot = 1;
			}
			r++;
		}
		r--;
		//last digit
		right = r;
		return stof(s.substr(ind + 1, r - ind));
	}

	int priority(char op) {
		if (op == '*' || op == '/') {
			return 2;
		}
		else if (op == '+' || op == '-') {
			return 1;
		}
		else {
			return 0;
		}
	}

	float calc(string s, int iter) {
		stack<float> nums;
		stack<char> ops;
		size_t sz = s.size();
		int i = 0;
		while (i < sz) {
			if (s[i] == '+' || s[i] == '-' || s[i] == '*' || s[i] == '/') {
				while (!ops.empty() && priority(s[i]) <= priority(ops.top())) {
					double num2 = nums.top();
					nums.pop();
					double num1 = nums.top();
					nums.pop();
					char op = ops.top();
					ops.pop();

					if (op == '+') {
						nums.push(num1 + num2);
					}
					else if (op == '-') {
						nums.push(num1 - num2);
					}
					else if (op == '*') {
						nums.push(num1 * num2);
					}
					else if (op == '/') {
						nums.push(num1 / num2);
					}
				}
				ops.push(s[i]);
			}
			else {
				float num = getRight(s, i - 1, i, iter);
				nums.push(num);
			}
			i++;
		}
		while (!ops.empty()) {
			double num2 = nums.top();
			nums.pop();
			double num1 = nums.top();
			nums.pop();
			char op = ops.top();
			ops.pop();

			if (op == '+') {
				nums.push(num1 + num2);
			}
			else if (op == '-') {
				nums.push(num1 - num2);
			}
			else if (op == '*') {
				nums.push(num1 * num2);
			}
			else if (op == '/') {
				if (num2 == 0)
					throw exception("Division by zero");
				else
					nums.push(num1 / num2);
			}
		}

		return nums.top();
	}

	string calculated(string s, int iter) {
		int ind;
		while ((ind = s.find("${")) != -1) {
			int endInd = ind;
			while (s[endInd] != '}')
				endInd++;
			float r = calc(s.substr(ind + 2, endInd - ind - 2), iter);
			s = s.substr(0, ind) + toStr(r) + s.substr(endInd + 1);
		}
		return s;
	}

	void doOut(string where, string what, int iter) {
		what = calculated(what, iter);
		if (where == "std") {
			int sz = what.size();
			char* wh = new char[sz + 1];
			for (int i = 0; i < sz; i++)
				wh[i] = what[i];
			wh[sz] = 0;
			printf("%s\n", wh);
			delete[] wh;
			//cout << what << "\n";
		}
		else {
			fstream outF(where, fstream::out | fstream::app);
			outF << what;
			outF << "\n";
			outF.close();
		}
	}

	void out() {
		string where, what;
		getline(f, where);
		getline(f, what);
		//thread th = thread(doOut, where, what);
		//th.detach();
		threads.push_back(thread(doOut, where, what, 0));
	}

	void doLoop(string commands, int st, int en) {
		int iter = st, sz = commands.size();
		for (iter; iter != en; (st > en) ? iter-- : iter++) {
			int i = 0;
			while (i < sz) {
				string s = "", what = "";
				while (commands[i] != '\n') {
					s += commands[i];
					i++;
				}
				if (s != "out")
					throw exception("Unsupported command inside a loop");
				s = "";
				i++;
				while (commands[i] != '\n') {
					s += commands[i];
					i++;
				}
				i++;
				while (commands[i] != '\n') {
					what += commands[i];
					i++;
				}
				i++;
				threads.push_back(thread(doOut, s, what, iter));
			}
		}
	}

	void loop(int st, int en) {
		string s = "", commands = "";
		getline(f, s);
		while (!f.eof() && s != "end loop")
		{
			commands += s + '\n';
			getline(f, s);
		}
		if (f.eof() && s != "end loop")
			throw exception("Loop not finished");
		doLoop(commands, st, en);
	}

	void doCommand() {
		string command;
		getline(f, command);
		if (command == "out")
			out();
		else if (command.substr(0, 4) == "loop")
		{
			int en, st = int(round(getRight(command, 4, en))), f;
			en = int(round(getRight(command, en + 1, f)));
			loop(st, en);
		}
		else
			throw exception("Unsupported command");
	}

	void work(string filename) {
		f.open(filename);
		while (!f.eof()) {
			doCommand();
		}
	}
};

int main() {
	myLang::work("text.txt");
	size_t sz = threads.size();
	for (int i = 0; i < sz; i++)
		threads[i].join();
	this_thread::sleep_for(std::chrono::milliseconds(1000));
	return 0;
}