import sys
import timeit
import time

def f(x):
	return x**2 - x**2 + 4 * x - 5 * x + x + x

if (len(sys.argv) != 2):
	print('Incorrect number of arguements')
	sys.exit()
try:
	n = int(sys.argv[1])
	if (n > 999999999):
		print('Too many symbols')
		sys.exit()
	st = timeit.default_timer()
	for i in range(0, n):
		f(834593)
	en = timeit.default_timer()
	print(f'Time taken is {en - st}s')
except ValueError:
	print('Not an integer passed')
