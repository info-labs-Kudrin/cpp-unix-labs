#include <iostream>
#include <chrono>

using namespace std;

int f(int x) {
	return x * x - x * x + x * 4 - x * 5 + x + x;
}

int getInt(char *arg) {
	int res = 0, i = 0;
	while (i < 9 && arg[i] != '\0') {
		if (arg[i] > '9' || arg[i] < '0')
			return -1;
		res = res * 10 + arg[i] - '0';
		i++;
	}
	if (arg[i] != '\0')
		return -2;
	return res;
}

int main(int c, char *args[]) {
	if (c == 1) {
		printf("You did not pass the number of iterations. Please, pass it like that:\n ./prog 5\nIn this cas the number is 5\n");
		return 0;
	}
	if (c > 2) {
		printf("Too many arguements passed\n");
		return 0;
	}
	int r = getInt(args[1]);
	if (r == -1) {
		printf("Not an integer passed\n");
		return 0;
	}
	if (r == -2) {
		printf("Too many symbols\n");
		return 0;
	}
	auto start = chrono::high_resolution_clock::now();
	for (int i = 0 ; i < r; i++)
		f(823475);
	auto finish = chrono::high_resolution_clock::now();
	chrono::duration<float> dur = (finish - start);
	int res = int(1000000.0 * dur.count());
	printf("The calculations took %d seconds, %d millisecond, %d microseconds\n", res / 1000000, res / 1000 % 1000, res % 1000);
	return 0;
}
