#include <iostream>
#include <fcntl.h>
#include <cstring>
#include <io.h>
#include <process.h>

#include <Windows.h>

int f1() {
	int x = 123;
	x = x * x - x * x + x * x * x * x - x * x * x * x * x + x + x;
	return x;
}

int main() {
	std::string lstr;
	HANDLE pipe = CreateNamedPipe("\\\\.\\pipe\\myPipe", PIPE_ACCESS_DUPLEX, PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT, PIPE_UNLIMITED_INSTANCES, 0, 0, 0, NULL);
	if (pipe == INVALID_HANDLE_VALUE) {
		// ��������� ������
		return 1;
	}
	char buffer[10]("Hello");
	DWORD bytesWrite;
	if (!WriteFile(pipe, (LPVOID)buffer, sizeof(buffer), &bytesWrite, NULL)) {}
	// ��� ��� ������ � ������� pipe
	std::cin >> lstr;
	CloseHandle(pipe);
	return 0;
}